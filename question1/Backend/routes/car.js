const express = require('express')
const db = require('../db')
const router = express.Router()
const utils = require('../utils')

router.get('/getcar', (request, response) => {
  const statement = `
        select * from car ;

    `
  db.pool.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.post('/add', (request, response) => {
  const { car_name, company_name, car_prize } = request.body

  const statement = `
        insert into car
            (car_name,company_name,car_prize)
        values
            (?,?,?)   
    `
  db.pool.query(
    statement,
    [car_name, company_name, car_prize],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.put('/update/:car_id', (request, response) => {
  const { car_id } = request.params
  const { car_name, company_name } = request.body

  const statement = `
        update car
            set
            car_name=? , company_name=?
            where
            car_id = ?
    `
  db.pool.query(
    statement,
    [car_name, company_name, car_id],
    (error, result) => {
      response.send(utils.createResult(error, result))
    }
  )
})

router.delete('/remove/:car_name', (request, response) => {
  const { car_name } = request.params
  const statement = `
        delete from car 
            where
            car_name = ?
    `
  db.pool.query(statement, [car_name], (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
module.exports = router
